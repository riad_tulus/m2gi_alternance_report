\select@language {french}
\contentsline {chapter}{\numberline {1}Introduction G\IeC {\'e}n\IeC {\'e}rale}{4}
\contentsline {chapter}{\numberline {2}Etat de l'art}{7}
\contentsline {section}{\numberline {2.1}Introduction}{7}
\contentsline {section}{\numberline {2.2}Concepts de base}{7}
\contentsline {subsection}{\numberline {2.2.1}Les acteurs principeaux}{7}
\contentsline {subsection}{\numberline {2.2.2}Maladie}{7}
\contentsline {subsection}{\numberline {2.2.3}Maternit\IeC {\'e}}{8}
\contentsline {subsection}{\numberline {2.2.4}Paternite}{8}
\contentsline {subsection}{\numberline {2.2.5}Adoption}{8}
\contentsline {section}{\numberline {2.3}Conclusion}{8}
\contentsline {chapter}{\numberline {3}Etude conceptuelle}{10}
\contentsline {section}{\numberline {3.1}Introduciton}{10}
\contentsline {section}{\numberline {3.2}Description Globale de la solution}{10}
\contentsline {section}{\numberline {3.3}D\IeC {\'e}marche globale}{10}
\contentsline {section}{\numberline {3.4}Socle fonctionnel de l'application}{11}
\contentsline {subsection}{\numberline {3.4.1}Faits g\IeC {\'e}n\IeC {\'e}rateurs}{12}
\contentsline {subsection}{\numberline {3.4.2}Eligibilit\IeC {\'e} et recevabilit\IeC {\'e}}{12}
\contentsline {subsection}{\numberline {3.4.3}Revenus}{13}
\contentsline {subsection}{\numberline {3.4.4}Liquidation}{13}
\contentsline {subsection}{\numberline {3.4.5}Ech\IeC {\'e}ance}{13}
\contentsline {subsection}{\numberline {3.4.6}Fiscalit\IeC {\'e}}{13}
\contentsline {subsection}{\numberline {3.4.7}Cl\IeC {\^o}ture}{14}
\contentsline {subsection}{\numberline {3.4.8}R\IeC {\'e}gularisation}{14}
\contentsline {subsection}{\numberline {3.4.9}Communication}{14}
\contentsline {section}{\numberline {3.5}Socle technique de l'application}{14}
\contentsline {chapter}{\numberline {4}R\IeC {\'e}alisation et Validation}{16}
\contentsline {chapter}{\numberline {5}Ma contribution}{17}
\contentsline {chapter}{\numberline {6}Conclusion G\IeC {\'e}n\IeC {\'e}rale}{18}
